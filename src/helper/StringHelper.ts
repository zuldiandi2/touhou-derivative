import Genders from 'src/assets/game/config/gender.json'

interface textBuffer {
    value: string
}

interface flagBuffer {
    value: boolean
}

export default function () {
    let intervalHandler: ReturnType<typeof setTimeout>

    function replaceAllParam(text: string, playerName: string, genderId: number): string {
        let replacedText
        replacedText = replacePlayerParam(text, playerName)
        replacedText = replaceGenderParam(replacedText, genderId)
        return replacedText
    }

    function replacePlayerParam(text: string, replacement: string): string {
        return text.replaceAll('@playerName@', replacement)
    }

    function replaceGenderParam(text: string, genderId: number): string {
        let replacedText
        const replacement = Genders.find((el) => el.value === genderId)
        replacedText = text.replaceAll('@playerGenderSecond@', replacement ? replacement.second : '')
        replacedText = replacedText.replaceAll('@playerGenderThird@', replacement ? replacement.third : '')

        return replacedText
    }

    function animatingShowText(text: string, bufferText: textBuffer, bufferFlag: flagBuffer) {
        bufferFlag.value = false
        let i = 0
        clearInterval(intervalHandler)
        intervalHandler = setInterval(() => {
            if (text) {
                if (i !== text.length) {
                    bufferText.value += text.substring(i, i + 1)
                    i++
                } else {
                    clearInterval(intervalHandler)
                    bufferFlag.value = true
                }
            } else {
                clearInterval(intervalHandler)
                bufferFlag.value = true
            }
        }, 50)
    }

    function forceCompleteText(replacedText: string, bufferText: textBuffer, bufferFlag: flagBuffer) {
        clearInterval(intervalHandler)
        bufferFlag.value = true
        bufferText.value = replacedText
    }

    return {
        replaceAllParam,
        replacePlayerParam,
        replaceGenderParam,
        animatingShowText,
        forceCompleteText
    }
}
