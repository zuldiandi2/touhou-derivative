import Languages from 'src/assets/game/config/language.json'
import { defineStore } from 'pinia'
import { computed } from 'vue'
import { useState } from './state'

export const useGetters = defineStore('game.global.getters', () => {
    const state = useState()

    const getLanguageText = computed((): string | undefined =>
        Languages.find((el) => el.value === state.languageId)?.label
    )

    return {
        getLanguageText
    }
})
