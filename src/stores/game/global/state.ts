import { defineStore } from 'pinia'

export interface State {
  languageId: number
}

export const useState = defineStore({
  id: 'game.global.state',

  state: (): State => {
    return {
        languageId: 0
    }
  }
})
