import { useState } from './state'
import { defineStore } from 'pinia'

export const useActions = defineStore('game.global.actions', () => {
    const state = useState()

    function setLanguage(langId: number): void {
        state.languageId = langId
    }

    // Note you are free to define as many internal functions as you want.
    // You only expose the functions that are returned.
    return {
        setLanguage
    }
})
