import Genders from 'src/assets/game/config/gender.json'
import { defineStore } from 'pinia'
import { computed } from 'vue'
import { useState } from './state'

export const useGetters = defineStore('game.player.getters', () => {
    const state = useState()

    const getPlayerName = computed((): string => state.playerName)

    const getPlayerGenderId = computed((): number => state.genderId)

    const getPlayerGenderText = computed((): string | undefined =>
        Genders.find((el) => el.value === state.genderId)?.label
    )

    return {
        getPlayerName,
        getPlayerGenderId,
        getPlayerGenderText
    }
})
