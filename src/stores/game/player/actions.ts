import { useState } from './state'
import { defineStore } from 'pinia'

export const useActions = defineStore('game.player.actions', () => {
    const state = useState()

    function setPlayerName(playerName: string): void {
        state.playerName = playerName
    }

    function setPlayerGender(genderId: number) {
        state.genderId = genderId
    }

    // Note you are free to define as many internal functions as you want.
    // You only expose the functions that are returned.
    return {
        setPlayerName,
        setPlayerGender
    }
})
