import { defineStore } from 'pinia'

export interface State {
    playerName: string,
    genderId: number
}

export const useState = defineStore({
    id: 'game.player.state',

    state: (): State => {
        return {
            // playerName: undefined,
            playerName: 'lol',
            genderId: 0
        }
    }
})
