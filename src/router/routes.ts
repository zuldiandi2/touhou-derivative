import { RouteRecordRaw } from 'vue-router'
import gameRoute from './game'

const routes: RouteRecordRaw[] = [
  {
    path: '/error',
    component: () => import('layouts/MainLayout.vue'),
    children: [{ path: '', component: () => import('pages/IndexPage.vue') }]
  },
  gameRoute,
  // Always leave this as last one,
  // but you can also remove it
  {
    path: '/:catchAll(.*)*',
    component: () => import('pages/ErrorNotFound.vue')
  }
]

export default routes
