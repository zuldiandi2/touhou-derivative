import { RouteRecordRaw } from 'vue-router'

const namePrefix = 'game'

const gameRoute: RouteRecordRaw = {
    path: '/',
    component: () => import('layouts/GameLayout.vue'),
    children: [
        {
            path: '',
            name: `${namePrefix}.menu`,
            component: () => import('src/pages/game/MainMenu.vue')
        },
        {
            path: 'about',
            name: `${namePrefix}.about`,
            component: () => import('src/pages/game/AboutPage.vue')
        },
        {
            path: 'intro',
            name: `${namePrefix}.intro`,
            component: () => import('src/pages/game/IntroPage.vue')
        },
        {
            path: 'start',
            name: `${namePrefix}.start`,
            component: () => import('src/pages/game/StartPage.vue'),
            meta: {
                needPlayerBio: true
            }
        }
    ]
}

export default gameRoute
